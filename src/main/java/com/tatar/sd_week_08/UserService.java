/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_08;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tatar
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;
    private static User superAdmin = new User("root","0000");
    static {
        //userList = new ArrayList<>();
        // Mock Data
        //userList.add(superAdmin);
        load();
    }

    // Create user
    public static boolean addUser(User user) {
        userList.add(user);
        save();
        return true;
    }

    // Delete user
    public static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }

    // Read user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    public static User getUser(int index) {
        return userList.get(index);
    }

    // Update user
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }

    // File 
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos=new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
            System.out.println();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis=new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static User logIn(String userName,String password){
        if(userName.equals(superAdmin.getUserName())&&password.equals(superAdmin.getPassword())){
            currentUser=superAdmin;
            return superAdmin;
        }
        for(int i=0;i<userList.size();i++){
            User user = userList.get(i);
            if(user.getUserName().equals(userName)&&user.getPassword().equals(password)){
                currentUser=user;
                return user;
            }
        }
        return null;
    }
    
    public static boolean isLogin(){
        return currentUser !=null;
    }
    public static User getCurrentUser(){
        return currentUser;
    }
    
    public static void logout(){
        currentUser = null;
    }
}
